package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

public class FootermenuHideTest extends BaseTest {
	@Test
	public void footerhidesearch() throws InterruptedException {
		
		WebDriver driver=new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		
		driver.manage().window().maximize();
		Thread.sleep(500);
		
		//login
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();
		
		//Footer menu bar hide and unhide
				driver.findElement(By.id(UIElementsIdentifier.FOOTER_MENU_BAR)).click();
				Thread.sleep(3000);
				driver.findElement(By.id(UIElementsIdentifier.FOOTER_MENU_BAR)).click();
				Thread.sleep(2000);
		
		
		
		driver.findElement(By.linkText(UIElementsIdentifier.LOGOUT_LINKTEXT)).click();
	}
}

