package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

public class AddToStockTest extends BaseTest {

	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	@Test
	public void addtostock() throws InterruptedException
	{
		WebDriver driver=new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		driver.manage().window().maximize();
		Thread.sleep(1500);
		
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();
		
		Actions builder = new Actions(driver);
		WebElement a=driver.findElement(By.name(UIElementsIdentifier.WORKQUEUE_MOUSEOVER_NAME));
		builder.moveToElement(a).build().perform();
		Thread.sleep(500);
		WebElement a1=driver.findElement(By.name(UIElementsIdentifier.LOTS_FOR_LABELLING));
		builder.moveToElement(a1).click().build().perform();
		Thread.sleep(2500);
		
		driver.findElement(By.xpath(UIElementsIdentifier.LOT_NUMBER)).click();
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript(UIElementsIdentifier.SCROLL1,IdmsAutomationConstants.STRINGSCROLLING);
		
		driver.findElement(By.xpath(UIElementsIdentifier.ADD_TO_STOCK)).click();
		driver.findElement(By.id(UIElementsIdentifier.QUANTITY)).sendKeys(dataSheet.getCell(1,47).getContents());
		driver.findElement(By.id(UIElementsIdentifier.NOTES)).sendKeys(dataSheet.getCell(1,48).getContents());
		driver.findElement(By.id(UIElementsIdentifier.SAVECLICK)).click();
		driver.findElement(By.id(UIElementsIdentifier.CONTINUE)).click();
		Thread.sleep(2500);
		jse.executeScript(UIElementsIdentifier.SCROLL1,IdmsAutomationConstants.STRINGSCROLLING);
		Thread.sleep(2500);
		
		
		driver.findElement(By.linkText(UIElementsIdentifier.LOGOUT_LINKTEXT)).click();
	}
}
