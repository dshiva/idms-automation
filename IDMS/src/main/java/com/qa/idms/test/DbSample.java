package com.qa.idms.test;

import java.sql.Connection;
import java.sql.DriverManager;
//import java.sql.ResultSet;
import java.sql.Statement;

public class DbSample {

	
	public static void main(String[] args) throws Exception {
		
		// connection url eg:"jdbc:mysql://ipaddress/port num/dbname"
		String dbUrl = "jdbc:mysql://192.168.100.10:3306/test";
		
		//username
		String username = "dev";
		//pwd
		String password = "dev";
		
		//give db query
		String query = "Insert Into Emp3 values('2','Shiva','Java')";
		
		
		//dropTable(tableNameList);
		
		//load mysql jdbc driver
		Class.forName("com.mysql.jdbc.Driver");	
	    //get connection to db
	    Connection con = DriverManager.getConnection(dbUrl,username,password);
	     //create statement obj
	     Statement stmt= con.createStatement();
	
	     //send sql query to db
	     //ResultSet rs = stmt.executeQuery(query);
	     stmt.executeUpdate(query);
	     //while to get all users
	      // while(rs.next()){
	          //  String username1 = rs.getString(1);
	          //  System.out.println(username1);
	               //          }
	       con.close();
		
	}

}
