package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

public class SystemTest extends BaseTest {
	
	@Test
	public void Systemadmin() throws InterruptedException
	{
		WebDriver driver=new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		driver.manage().window().maximize();
		Thread.sleep(1500);
		
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();
		
		
		Actions builder = new Actions(driver);
		WebElement a=driver.findElement(By.xpath(UIElementsIdentifier.SYSTEM));
		builder.moveToElement(a).build().perform();
		Thread.sleep(500);
		WebElement a1=driver.findElement(By.xpath(UIElementsIdentifier.WORKLOAD_STATISTICS));
		builder.moveToElement(a1).click().build().perform();
		Thread.sleep(2500);
		driver.findElement(By.xpath(UIElementsIdentifier.WORKLOAD_STATISTICS_SEARCH)).click();
		Thread.sleep(2500);
		
		WebElement b=driver.findElement(By.xpath(UIElementsIdentifier.SYSTEM));
		builder.moveToElement(b).build().perform();
		Thread.sleep(500);
		WebElement b1=driver.findElement(By.xpath(UIElementsIdentifier.IRREGULARITY_CONFIRMATIONS));
		builder.moveToElement(b1).click().build().perform();
		Thread.sleep(2500);
		driver.findElement(By.xpath(UIElementsIdentifier.IRREGULARITY_CONFIRMATIONS_SEARCH)).click();
		Thread.sleep(2500);
		
		WebElement c=driver.findElement(By.xpath(UIElementsIdentifier.SYSTEM));
		builder.moveToElement(c).build().perform();
		Thread.sleep(500);
		WebElement c1=driver.findElement(By.xpath(UIElementsIdentifier.PREFERENCES));
		builder.moveToElement(c1).click().build().perform();
		Thread.sleep(1500);
		driver.findElement(By.id(UIElementsIdentifier.PREFERENCES_THEME)).click();
		Thread.sleep(1500);
		driver.findElement(By.id(UIElementsIdentifier.PREFERENCES_THEME_SAVE)).click();
		Thread.sleep(1500);
		
		WebElement d=driver.findElement(By.xpath(UIElementsIdentifier.SYSTEM));
		builder.moveToElement(d).build().perform();
		Thread.sleep(500);
		WebElement d1=driver.findElement(By.xpath(UIElementsIdentifier.SYSTEMINFO));
		builder.moveToElement(d1).click().build().perform();
		Thread.sleep(1500);
		
		
		WebElement e=driver.findElement(By.xpath(UIElementsIdentifier.SYSTEM));
		builder.moveToElement(e).build().perform();
		Thread.sleep(500);
		WebElement e1=driver.findElement(By.xpath(UIElementsIdentifier.HELP));
		builder.moveToElement(e1).click().build().perform();
		Thread.sleep(1500);
		
		WebElement f=driver.findElement(By.xpath(UIElementsIdentifier.SYSTEM));
		builder.moveToElement(f).build().perform();
		Thread.sleep(500);
		WebElement f1=driver.findElement(By.xpath(UIElementsIdentifier.ADMIN));
		builder.moveToElement(f1).build().perform();
		Thread.sleep(500);
		WebElement f2=driver.findElement(By.xpath(UIElementsIdentifier.MANAGE_USERS));
		builder.moveToElement(f2).click().build().perform();
		Thread.sleep(1500);
		driver.findElement(By.xpath(UIElementsIdentifier.MANAGE_USERS_SEARCH)).click();
		Thread.sleep(1500);
		
		WebElement g=driver.findElement(By.xpath(UIElementsIdentifier.SYSTEM));
		builder.moveToElement(g).build().perform();
		Thread.sleep(500);
		WebElement g1=driver.findElement(By.xpath(UIElementsIdentifier.ADMIN));
		builder.moveToElement(g1).build().perform();
		Thread.sleep(500);
		WebElement g2=driver.findElement(By.xpath(UIElementsIdentifier.ADD_USER));
		builder.moveToElement(g2).click().build().perform();
		Thread.sleep(1500);
		
		
		WebElement h=driver.findElement(By.xpath(UIElementsIdentifier.SYSTEM));
		builder.moveToElement(h).build().perform();
		Thread.sleep(500);
		WebElement h1=driver.findElement(By.xpath(UIElementsIdentifier.ADMIN));
		builder.moveToElement(h1).build().perform();
		Thread.sleep(500);
		WebElement h2=driver.findElement(By.xpath(UIElementsIdentifier.CRIS_RX_PROCESS));
		builder.moveToElement(h2).click().build().perform();
		Thread.sleep(1500);
		
		
		WebElement i=driver.findElement(By.xpath(UIElementsIdentifier.SYSTEM));
		builder.moveToElement(i).build().perform();
		Thread.sleep(500);
		WebElement i1=driver.findElement(By.xpath(UIElementsIdentifier.ADMIN));
		builder.moveToElement(i1).build().perform();
		Thread.sleep(500);
		WebElement i2=driver.findElement(By.xpath(UIElementsIdentifier.MANAGE_DATATYPES));
		builder.moveToElement(i2).click().build().perform();
		Thread.sleep(1500);
		
		
		
		
	}
}
