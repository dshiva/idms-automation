package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

public class CreateStudyTest extends BaseTest {

	
	@Test
	public void createstudy() throws InterruptedException {
		
		WebDriver driver=new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		driver.manage().window().maximize();
		Thread.sleep(500);
		
		//login
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();

		Actions builder = new Actions(driver);
		WebElement a=driver.findElement(By.name(UIElementsIdentifier.PROTOCOLS_MOUSEOVER_NAME));
		builder.moveToElement(a).build().perform();
		Thread.sleep(500);
		WebElement a1=driver.findElement(By.name(UIElementsIdentifier.SEARCHPROTOCOL));
		builder.moveToElement(a1).click().build().perform();
		Thread.sleep(500);
		
		driver.findElement(By.id(UIElementsIdentifier.PROTOCOLNUMBER)).sendKeys(dataSheet.getCell(1,16).getContents());
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
	    jse.executeScript(UIElementsIdentifier.SCROLL1,IdmsAutomationConstants.STRINGSCROLLING);
	    
	    driver.findElement(By.name(UIElementsIdentifier.SEARCH)).click();
	    
	    
	    driver.findElement(By.xpath(UIElementsIdentifier.RELATEDSTUDIES)).click();
		
	    driver.findElement(By.xpath(UIElementsIdentifier.ADDNEWSTUDY)).click();
	    
	    driver.findElement(By.id(UIElementsIdentifier.TITLE)).sendKeys(dataSheet.getCell(1,17).getContents());
	    JavascriptExecutor jse1 = (JavascriptExecutor)driver;
	    jse1.executeScript(UIElementsIdentifier.SCROLL,IdmsAutomationConstants.STRINGSCROLLING);
	    
	    driver.findElement(By.id(UIElementsIdentifier.ADD)).click();
	    
	    Thread.sleep(4000);
	    
	    driver.findElement(By.linkText(UIElementsIdentifier.LOGOUT_LINKTEXT)).click();
	    
	    
	    
	    
	}

}
