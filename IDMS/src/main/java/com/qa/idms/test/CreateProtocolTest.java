package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

/**
 * 
 * @author cps1
 *
 */
public class CreateProtocolTest extends BaseTest {
	
	@Test
	public void createprotocol() throws InterruptedException {
		WebDriver driver=new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		driver.manage().window().maximize();
		Thread.sleep(1500);
		
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();

		Actions builder = new Actions(driver);
		WebElement a=driver.findElement(By.name(UIElementsIdentifier.PROTOCOLS_MOUSEOVER_NAME));
		builder.moveToElement(a).build().perform();
		Thread.sleep(500);
		WebElement a1=driver.findElement(By.name(UIElementsIdentifier.CREATE_PROTOCOL_NAME));
		builder.moveToElement(a1).click().build().perform();
		Thread.sleep(2500);

		//initially trying for draft no by default giving protocol number directly
		//driver.findElement(By.id(UIElementsIdentifier.ISDRAFT_ID)).click();
		driver.findElement(By.id(UIElementsIdentifier.PROTOCOL_NUMBER)).sendKeys(dataSheet.getCell(1,4).getContents());

		Select listbox = new Select(driver.findElement(By.id(UIElementsIdentifier.STATE_SELECT_ID)));
		listbox.selectByVisibleText(dataSheet.getCell(1,5).getContents());

		driver.findElement(By.id(UIElementsIdentifier.PROTOCOL_TITILE_ID)).sendKeys(dataSheet.getCell(1,6).getContents());
		driver.findElement(By.id(UIElementsIdentifier.CLICK_ADD_ID)).click();
		
		Thread.sleep(5000);

		driver.findElement(By.linkText(UIElementsIdentifier.LOGOUT_LINKTEXT)).click();

	}

}
