package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

public class CreateLotTest extends BaseTest {

	@Test
	

	public void createlot() throws InterruptedException {
		WebDriver driver=new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		driver.manage().window().maximize();
		Thread.sleep(500);

		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();
		
		Actions builder = new Actions(driver);
		WebElement a=driver.findElement(By.name(UIElementsIdentifier.INVENTORYMENU));
		builder.moveToElement(a).build().perform();
		Thread.sleep(500);
		WebElement a1=driver.findElement(By.name(UIElementsIdentifier.INVENTORYCREATE));
		builder.moveToElement(a1).click().build().perform();
		Thread.sleep(500);
		
		driver.findElement(By.id(UIElementsIdentifier.ORDERDRUGRADIOBUTTON)).click();
		
		driver.findElement(By.id(UIElementsIdentifier.DRUGNAME)).sendKeys(dataSheet.getCell(1,28).getContents());
		
		driver.findElement(By.id(UIElementsIdentifier.NEXT3)).click();
		Thread.sleep(1500);
		
		driver.findElement(By.id(UIElementsIdentifier.LOTSTRENGTH)).sendKeys(dataSheet.getCell(1,29).getContents());
		
		Select listbox = new Select(driver.findElement(By.id(UIElementsIdentifier.STRENGTHUNIT)));
		listbox.selectByVisibleText(dataSheet.getCell(1,30).getContents());
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
	    jse.executeScript(UIElementsIdentifier.SCROLL1,IdmsAutomationConstants.STRINGSCROLLING);
	    
	    driver.findElement(By.id(UIElementsIdentifier.NEXT2)).click();
	    Thread.sleep(5000);

		driver.findElement(By.linkText(UIElementsIdentifier.LOGOUT_LINKTEXT)).click();
	    
	
	}

}
