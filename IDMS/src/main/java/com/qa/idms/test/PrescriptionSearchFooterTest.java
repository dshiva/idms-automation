package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

public class PrescriptionSearchFooterTest extends BaseTest {
	@Test
	public void prescriptionsearch() throws InterruptedException {
		
		WebDriver driver=new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		
		driver.manage().window().maximize();
		Thread.sleep(500);
		
		//login
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();
		
		//Prescription Search overall and then individual
		driver.findElement(By.xpath(UIElementsIdentifier.PRESCRIPTION_FOOTER_SEARCH)).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(UIElementsIdentifier.HOME)).click();
		Thread.sleep(1500);
		driver.findElement(By.id(UIElementsIdentifier.PRESCRIPTION_FOOTER_NUMBER)).sendKeys(dataSheet.getCell(1,49).getContents());
		driver.findElement(By.xpath(UIElementsIdentifier.PRESCRIPTION_FOOTER_SEARCH)).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath(UIElementsIdentifier.HOME)).click();
		Thread.sleep(1500);
		
		
		
		driver.findElement(By.linkText(UIElementsIdentifier.LOGOUT_LINKTEXT)).click();
	}
}
