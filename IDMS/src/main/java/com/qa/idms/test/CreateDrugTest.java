package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

public class CreateDrugTest extends BaseTest {

	@Test
	public void createdrug() throws InterruptedException {
		
		WebDriver driver = new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		driver.manage().window().maximize();
		Thread.sleep(500);
		
		//login
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();

		Actions builder = new Actions(driver);
		WebElement a=driver.findElement(By.name(UIElementsIdentifier.DRUGS_MOUSEOVER_NAME));
		builder.moveToElement(a).build().perform();
		Thread.sleep(1500);
		WebElement a1=driver.findElement(By.name(UIElementsIdentifier.CREATE_DRUG_NAME));
		builder.moveToElement(a1).click().build().perform();
		Thread.sleep(1500);

		driver.findElement(By.id(UIElementsIdentifier.DRUG_ASSOCIATED_PROTOCOL_TRUE_ID)).click();
		driver.findElement(By.id(UIElementsIdentifier.PROTOCOL_NUMBER_ID)).sendKeys(dataSheet.getCell(1,8).getContents());

		driver.findElement(By.id(UIElementsIdentifier.CLICK_NEXT_ID)).click();

		driver.findElement(By.id(UIElementsIdentifier.DRUG_COMPOUND_ID)).sendKeys(dataSheet.getCell(1,9).getContents());
		Select listbox = new Select(driver.findElement(By.id(UIElementsIdentifier.DOSAGE_FORM_ID)));
		listbox.selectByVisibleText(dataSheet.getCell(1,10).getContents());
		driver.findElement(By.id(UIElementsIdentifier.STRENGTH_UNKNOWN_ID)).click();

		driver.findElement(By.id(UIElementsIdentifier.NEW_DRUG_NAME)).sendKeys(dataSheet.getCell(1,11).getContents());
		driver.findElement(By.id(UIElementsIdentifier.DRUG_CODE)).sendKeys(dataSheet.getCell(1,12).getContents());
		driver.findElement(By.id(UIElementsIdentifier.DRUG_MANUFACTURER_NAME)).sendKeys(dataSheet.getCell(1,13).getContents());
		Thread.sleep(1500);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,550)","");

		driver.findElement(By.id(UIElementsIdentifier.NEXT)).click();
		driver.findElement(By.id(UIElementsIdentifier.SAVE)).click();
		Thread.sleep(5000);
		
		//Changing to active mode of drug
		
		//JavascriptExecutor jse1 = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,850)","");
		driver.findElement(By.id(UIElementsIdentifier.EDIT)).click();
		
		listbox = new Select(driver.findElement(By.id(UIElementsIdentifier.DRUG_EDIT_STATE)));
		listbox.selectByVisibleText(dataSheet.getCell(1,14).getContents());
		jse.executeScript("window.scrollBy(0,850)","");
		driver.findElement(By.id(UIElementsIdentifier.SAVE)).click();
		
		Thread.sleep(5000);
		
		
		driver.findElement(By.linkText(UIElementsIdentifier.LOGOUT_LINKTEXT)).click();
	}
}
