package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

public class StudySetupTest extends BaseTest {

	@Test
	
	
	public void studysetup() throws InterruptedException {
		WebDriver driver=new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		driver.manage().window().maximize();
		Thread.sleep(500);

		//login
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();

		Actions builder = new Actions(driver);
		WebElement a=driver.findElement(By.name(UIElementsIdentifier.PROTOCOLS_MOUSEOVER_NAME));
		builder.moveToElement(a).build().perform();
		Thread.sleep(500);
		WebElement a1=driver.findElement(By.name(UIElementsIdentifier.SEARCHPROTOCOL));
		builder.moveToElement(a1).click().build().perform();
		Thread.sleep(500);
		
        driver.findElement(By.id(UIElementsIdentifier.PROTOCOLNUMBER)).sendKeys(dataSheet.getCell(1,20).getContents());
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
	    jse.executeScript(UIElementsIdentifier.SCROLL1,IdmsAutomationConstants.STRINGSCROLLING);
	    Thread.sleep(500);
	    driver.findElement(By.name(UIElementsIdentifier.SEARCH)).click();
	    
	    driver.findElement(By.xpath(UIElementsIdentifier.RELATEDSTUDIES)).click();
	    Thread.sleep(1500);
	    
	    driver.findElement(By.id(UIElementsIdentifier.MODIFIED_DATE)).click();
	    Thread.sleep(1500);
	    
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONSTUDYTITLE)).click();
	    Thread.sleep(500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONSTUDYSETUP)).click();
	    Thread.sleep(500);
	    JavascriptExecutor jse1 = (JavascriptExecutor)driver;
	    jse1.executeScript(UIElementsIdentifier.SCROLL1,IdmsAutomationConstants.STRINGSCROLLING);
	    Thread.sleep(500);
	    driver.findElement(By.id(UIElementsIdentifier.EDIT)).click();
	    
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONPLUS)).click();
	    driver.findElement(By.id(UIElementsIdentifier.COMPOUNDSETNAME)).sendKeys(dataSheet.getCell(1,21).getContents());
	    Thread.sleep(500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONADD)).click();
	    Thread.sleep(1500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONPLUS)).click();
	    driver.findElement(By.id(UIElementsIdentifier.COMPOUNDSETNAME)).sendKeys(dataSheet.getCell(1,22).getContents());
	    Thread.sleep(500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONADD)).click();
	    Thread.sleep(500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONAPPLY)).click();
	    Thread.sleep(1500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONCOURSESUBCOURSETAB)).click();
	    Thread.sleep(500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONPLUS1)).click();
	    driver.findElement(By.id(UIElementsIdentifier.COURSENAME)).sendKeys(dataSheet.getCell(1,23).getContents());
	    Thread.sleep(500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONADD)).click();
	    Thread.sleep(500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONSTRATIFICATIONTAB)).click();
	    Thread.sleep(500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONSTRATIFICATIONPLUS)).click();
	    Thread.sleep(500);
	    driver.findElement(By.id(UIElementsIdentifier.STRATIFICATIONNAME)).sendKeys(dataSheet.getCell(1,24).getContents());
	    
	    driver.findElement(By.id(UIElementsIdentifier.STRATIFICATIONRANGESTART)).sendKeys(dataSheet.getCell(1,25).getContents());
	    driver.findElement(By.id(UIElementsIdentifier.STRATIFICATIONRANGEEND)).sendKeys(dataSheet.getCell(1,26).getContents());
	    Thread.sleep(500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONADD)).click();
	    Thread.sleep(500);
	    driver.findElement(By.xpath(UIElementsIdentifier.CLICKONTREATMENTASSIGNMENTSTAB)).click();
	    Thread.sleep(500);
	    
	    driver.findElement(By.id(UIElementsIdentifier.SAVEALLCHANGES)).click();
	    
        Thread.sleep(10000);
	    
	    driver.findElement(By.linkText(UIElementsIdentifier.LOGOUT_LINKTEXT)).click();
	    
	    
	}

}
