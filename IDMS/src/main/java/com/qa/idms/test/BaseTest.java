package com.qa.idms.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import com.qa.idms.common.IdmsAutomationConstants;

public class BaseTest {

	protected Sheet dataSheet;

	public BaseTest() {
		try {
			getSheet();
		} catch (BiffException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return s
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws BiffException
	 */
	private Sheet getSheet() throws FileNotFoundException, IOException, BiffException {
		InputStream fis = new FileInputStream(getFile(IdmsAutomationConstants.RESOURCE_FILE_NAME));
		Workbook wb = Workbook.getWorkbook(fis);
		Sheet dataSheet = wb.getSheet(0);
		this.dataSheet = dataSheet;
		return dataSheet;
	}

	/**
	 *  Read a file from resources directory.
	 * @param fileName
	 * @return
	 */
	private File getFile(String fileName) {
		//Get file from resources folder
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());
		return file;
	}
}
