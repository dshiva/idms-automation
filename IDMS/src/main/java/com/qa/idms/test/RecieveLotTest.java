package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

public class RecieveLotTest extends BaseTest {

	@Test
	public void receivelot() throws InterruptedException {
		WebDriver driver=new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		driver.manage().window().maximize();
		Thread.sleep(500);

		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();
		
		Actions builder = new Actions(driver);
		WebElement a=driver.findElement(By.name(UIElementsIdentifier.WORKQUEUEMENU));
		builder.moveToElement(a).build().perform();
		Thread.sleep(500);
		WebElement a1=driver.findElement(By.name(UIElementsIdentifier.LOTSONORDER));
		builder.moveToElement(a1).click().build().perform();
		Thread.sleep(500);
		
		//to get recent working lot , sort by queued date
		
		driver.findElement(By.id(UIElementsIdentifier.QUEUED_DATE)).click();
		Thread.sleep(1500);
		driver.findElement(By.id(UIElementsIdentifier.QUEUED_DATE)).click();
		Thread.sleep(1500);
		
		
		driver.findElement(By.xpath(UIElementsIdentifier.CLICKONLOTNUMBER)).click();
		Thread.sleep(3000);
		driver.findElement(By.id(UIElementsIdentifier.LOWBALANCE)).sendKeys(dataSheet.getCell(1,32).getContents());
		Thread.sleep(3000);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript(UIElementsIdentifier.SCROLL1,IdmsAutomationConstants.STRINGSCROLLING);	
		
		
		driver.findElement(By.id(UIElementsIdentifier.CLICKONRECEIVE)).click();
		Thread.sleep(3000);
		driver.findElement(By.id(UIElementsIdentifier.SPECIALLOTNUMBER)).sendKeys(dataSheet.getCell(1,33).getContents());
		Thread.sleep(3000);
		driver.findElement(By.id(UIElementsIdentifier.MANUFACTURERLOTNUMBER)).sendKeys(dataSheet.getCell(1,34).getContents());
		driver.findElement(By.id(UIElementsIdentifier.INITIALBALANCE)).sendKeys(dataSheet.getCell(1,35).getContents());
		Select listbox = new Select(driver.findElement(By.id(UIElementsIdentifier.ACCOUNTABILITYUNITTYPE)));
		listbox.selectByVisibleText(dataSheet.getCell(1,36).getContents());
		
		
		JavascriptExecutor jse2 = (JavascriptExecutor)driver;
		jse2.executeScript(UIElementsIdentifier.SCROLL1,IdmsAutomationConstants.STRINGSCROLLING);
		Thread.sleep(1000);
		
		driver.findElement(By.id(UIElementsIdentifier.EXPIRATIONDATE)).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(UIElementsIdentifier.SELECTMONTHFIELD)).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath(UIElementsIdentifier.SELECTDATE)).click();
		Thread.sleep(2000);
		driver.findElement(By.id(UIElementsIdentifier.ADVANCEWARNING)).sendKeys(dataSheet.getCell(1,37).getContents());
		Thread.sleep(1000);
		driver.findElement(By.id(UIElementsIdentifier.SAVELOTSUPPLY)).click();
		
		Thread.sleep(5000);

		driver.findElement(By.linkText(UIElementsIdentifier.LOGOUT_LINKTEXT)).click();
		
		
	}

}
