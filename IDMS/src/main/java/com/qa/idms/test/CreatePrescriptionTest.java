package com.qa.idms.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.qa.idms.common.IdmsAutomationConstants;
import com.qa.idms.common.UIElementsIdentifier;

public class CreatePrescriptionTest extends BaseTest {

	@Test
	public void createprescription() throws InterruptedException {
		WebDriver driver=new FirefoxDriver();
		driver.get(IdmsAutomationConstants.IDMS_LOGIN_PAGE);
		driver.manage().window().maximize();
		Thread.sleep(500);

		driver.findElement(By.id(UIElementsIdentifier.LOGIN_USERNAME_ID)).sendKeys(dataSheet.getCell(1,1).getContents());
		driver.findElement(By.id(UIElementsIdentifier.LOGIN_PASSWORD_ID)).sendKeys(dataSheet.getCell(1,2).getContents());
		driver.findElement(By.name(UIElementsIdentifier.LOGIN_NAME)).click();
		
		Actions builder = new Actions(driver);
		WebElement a=driver.findElement(By.name(UIElementsIdentifier.PRESCRIPTIONSMENU));
		builder.moveToElement(a).build().perform();
		Thread.sleep(2500);
		
		WebElement a1=driver.findElement(By.name(UIElementsIdentifier.PRESCRIPTIONCREATE));
		builder.moveToElement(a1).click().build().perform();
		Thread.sleep(500);
		
		driver.findElement(By.xpath(UIElementsIdentifier.CLICKNEXT)).click();
		Thread.sleep(1500);
		driver.findElement(By.id(UIElementsIdentifier.MEDICALRECORDNUMBER)).sendKeys(dataSheet.getCell(1,39).getContents());
		driver.findElement(By.id(UIElementsIdentifier.CRISORDERNUMBER)).sendKeys(dataSheet.getCell(1,40).getContents());
		driver.findElement(By.id(UIElementsIdentifier.CLICKPROTOCOLNUMBER)).sendKeys(dataSheet.getCell(1,41).getContents());
		
		driver.findElement(By.id(UIElementsIdentifier.RESETDRUG)).click();
		Thread.sleep(500);
		driver.findElement(By.id(UIElementsIdentifier.DRUGNAMETEXT)).sendKeys(dataSheet.getCell(1,42).getContents());
		Thread.sleep(1000);
		driver.findElement(By.id(UIElementsIdentifier.CLICKADDDRUG)).click();
		Thread.sleep(2500);
		
		driver.findElement(By.id(UIElementsIdentifier.PRESCRIBEDBY)).sendKeys(dataSheet.getCell(1,43).getContents());
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript(UIElementsIdentifier.SCROLL1,IdmsAutomationConstants.STRINGSCROLLING);
	    
	    driver.findElement(By.id(UIElementsIdentifier.CLICKONNEXT2)).click();
	    
	    driver.findElement(By.id(UIElementsIdentifier.DRUGSTRENGTH)).sendKeys(dataSheet.getCell(1,44).getContents());
	    Select listbox = new Select(driver.findElement(By.id(UIElementsIdentifier.STRENGTHUNITLISTBOX)));
		listbox.selectByVisibleText(dataSheet.getCell(1,45).getContents());
		driver.findElement(By.id(UIElementsIdentifier.CLICKNEXT3)).click();
		Thread.sleep(1500);
		driver.findElement(By.id(UIElementsIdentifier.CLICKONSAVE)).click();
		
		Thread.sleep(5000);

		driver.findElement(By.linkText(UIElementsIdentifier.LOGOUT_LINKTEXT)).click();

		
	}

}
