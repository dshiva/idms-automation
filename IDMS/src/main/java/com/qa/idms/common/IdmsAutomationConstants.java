package com.qa.idms.common;

/**
 * 
 * @author cps1
 *
 */
public class IdmsAutomationConstants {

	public static final String RESOURCE_FILE_NAME ="Selenium.xls";
	
	// Modify the below IP only if the machine IP gets changed
	public static final String IDMS_HOST = "192.168.100.10";
	public static final String IDMS_PORT = "8080";
	public static final String IDMS_LOGIN_PAGE = "http://"+IDMS_HOST+":"+IDMS_PORT+"/idms-app-1.5/";
//	public static final String LOGIN_USERNAME ="gadabala";
//	public static final String LOGIN_PASSWORD = "password";
	
	//public static final String STATE_ACTIVE = "Active";
	//public static final String PROTOCOL_DESCRIPTION = "Test Protocol Title inserted by Automation scripts";
	//public static final String PROTOCOL_NUMBER = "16-C-X003";
//	public static final String DRUG_COMPOUND_NAME = "EBASTINE";
//	public static final String DOSAGE_FORM_NAME = "Capsule";
//	public static final String DRUG_MANUFACTURER_NAME = "glasky";
//	public static final String DRUG_CODE = "ebas";
//	public static final String DRUG_NAME = "ebastine";
	
	//public static final String STUDYTITLE = "teststudy2";
	//public static final String NUMBER = "16-C-X003";
	public static final String STRINGSCROLLING = "";
	
//	public static final String ENDINGRANGENUMBER = "100";
//	public static final String STARTINGRANGENUMBER = "1";
//	public static final String NAMEOFSTRATIFICATION = "patients";
//	public static final String NAMEOFCOURSE = "Course1";
	//public static final String NAMEOFCOMPOUND2 = "Visit2";
	//public static final String NAMEOFCOMPOUND = "Visit1";
	
	//public static final String VALUEINDROPDOWN = "mg";
	//public static final String STRENGTHOFLOT = "100";
	//public static final String NAMEOFDRUG = "ebas";
	
//	public static final String WARNINGDAYSNUMBER = "10";
//	public static final String SELECTSTRENGTHINDROPDOWN = "mg";
//	public static final String INITIALBALANCENUMBER = "100";
//	public static final String MANUFACTURERLOTNUMBER = "201";
//	public static final String SPECAILLOTNUMBER = "123";
//	public static final String LOWBALANCENUMBER = "50";
	
//	public static final String SELECTSTRENGHINLISTBOX = "mg";
//	public static final String DRUGSTRENGTHNUMBER = "50";
//	public static final String DOCTORNAME = "Kim";
//	public static final String DRUGNAME = "ebas";
//	public static final String ORDERNUMBER = "82";
//	public static final String PATIENTNUMBER = "00-22-47-4";
	
	

}


//http://166.62.123.25:8085/idms-app-1.5/
//http://192.168.100.10:8080/idms-app-1.5/