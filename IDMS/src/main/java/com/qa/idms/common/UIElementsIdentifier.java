package com.qa.idms.common;

public class UIElementsIdentifier {
	
	public static final String LOGIN_USERNAME_ID = "j_username";
	public static final String LOGIN_PASSWORD_ID = "j_password";
	public static final String LOGIN_NAME = "login";
	public static final String LOGOUT_LINKTEXT = "Logout";
	
	public static final String PROTOCOLS_MOUSEOVER_NAME = "protocols-menu";
	public static final String CREATE_PROTOCOL_NAME = "protocol-add";
	public static final String ISDRAFT_ID = "draftTrue";
	public static final String PROTOCOL_NUMBER = "cpsProtocolNumber";
	public static final String STATE_SELECT_ID = "lifeCycleStateType";
	public static final String PROTOCOL_TITILE_ID = "fullTitle";
	public static final String CLICK_ADD_ID = "add";
	
	public static final String DRUGS_MOUSEOVER_NAME = "drugs-menu";
	public static final String DRUGS_COMPOUNDS_NAME = "drugcompounds-menu";
	public static final String DRUGS_COMPOUNDS_ADD_NAME = "drugcompound-add";
	public static final String DRUGS_MANUFACTURERS_NAME = "drugmanufacturers-menu";
	public static final String DRUGS_MANUFACTURERS_ADD_NAME = "drugmanufacturer-add";
	public static final String MANUFACTURER_NAME_ID = "manufacturerName";
	public static final String CREATE_DRUG_NAME = "drug-create";
	public static final String DRUG_ASSOCIATED_PROTOCOL_TRUE_ID = "associatedWithExistingProtocolTrue";
	public static final String PROTOCOL_NUMBER_ID = "protocolNumber";
	public static final String COMPOUND_NAME_ID = "name";
	public static final String CLICK_NEXT_ID = "next";
	public static final String EXISTING_COMPOUND_NO_NAME = "no";
	public static final String DRUG_COMPOUND_ID = "drugCompoundName";
	public static final String DOSAGE_FORM_ID = "dosageForm";
	public static final String STRENGTH_UNKNOWN_ID = "strengthChoiceUnknown";
	public static final String DRUG_CODE = "newDrugCode";
	public static final String NEW_DRUG_NAME = "newDrugName";
	public static final String DRUG_MANUFACTURER_NAME = "drugManufacturerName";
	public static final String SAVE = "save";
	public static final String NEXT = "next2";
	public static final String DRUG_EDIT_STATE = "lifeCycleStateType";
	
	public static final String SEARCH = "search";
	public static final String PROTOCOLNUMBER = "protocolNumber";
	public static final String SEARCHPROTOCOL = "protocol-search";
	public static final String SCROLL1 = "window.scrollBy(0,1000)";
	public static final String SCROLL = "window.scrollBy(0,1050)";
	public static final String ADD = "add";
	public static final String TITLE = "title";
	public static final String ADDNEWSTUDY = "/html/body/div[1]/div[7]/div[4]/div/div/div[1]/a";
	
	public static final String RELATEDSTUDIES = "//ul[@class='ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all']/li[2]";
	public static final String MODIFIED_DATE = "jqgh_dateModified";
	
	
	public static final String COURSENAME = "courseAddTopLevelCourseDO.name";
	public static final String CLICKONPLUS1 = "/html/body/div[1]/div[7]/div[4]/fieldset/div[2]/div[2]/div[2]/div[1]/div/table/tbody/tr/td/div/div[2]/button";
	public static final String CLICKONCOURSESUBCOURSETAB = "/html/body/div[1]/div[7]/div[4]/fieldset/div[2]/ul/li[2]/a";
	public static final String CLICKONAPPLY = "/html/body/div[1]/div[7]/div[3]/fieldset/div[2]/div[1]/div[2]/form/div[1]/div[2]/fieldset/input[1]";
	public static final String CLICKONADD = "/html/body/div[14]/div[2]/form/fieldset/div[3]/fieldset/input[1]";
	public static final String COMPOUNDSETNAME = "compoundSetEditDO.name";
	public static final String CLICKONPLUS = "/html/body/div[1]/div[7]/div[3]/fieldset/div[2]/div[1]/div[2]/form/div[1]/table/thead/tr[2]/th[4]/div/div[2]/button";
	public static final String EDIT = "edit";
	public static final String CLICKONSTUDYSETUP = "/html/body/div[1]/div[7]/div[4]/div/ul/li[3]/a";
	public static final String CLICKONSTUDYTITLE = "/html/body/div[1]/div[7]/div[4]/div/div/div[3]/div[1]/div/div[3]/div[3]/div/table/tbody/tr[1]/td[2]/a";
	public static final String SAVEALLCHANGES = "save-all-changes";
	public static final String CLICKONTREATMENTASSIGNMENTSTAB = "/html/body/div[1]/div[7]/div[3]/fieldset/div[2]/ul/li[4]/a";
	public static final String STRATIFICATIONRANGEEND = "stratificationRangeEditDO.rangeEnd";
	public static final String STRATIFICATIONRANGESTART = "stratificationRangeEditDO.rangeStart";
	public static final String STRATIFICATIONNAME = "stratificationRangeEditDO.name";
	public static final String CLICKONSTRATIFICATIONPLUS = "/html/body/div[1]/div[7]/div[3]/fieldset/div[2]/div[3]/div[2]/div[1]/table/tbody/tr[2]/td/div/button";
	public static final String CLICKONSTRATIFICATIONTAB = "/html/body/div[1]/div[7]/div[3]/fieldset/div[2]/ul/li[3]/a";
	
	public static final String NEXT2 = "next2";
	public static final String STRENGTHUNIT = "strengthUnit";
	public static final String LOTSTRENGTH = "lotStrength";
	public static final String NEXT3 = "next";
	public static final String DRUGNAME = "selectedDrugNameOrder";
	public static final String ORDERDRUGRADIOBUTTON = "orderDrug";
	public static final String INVENTORYCREATE = "inventory-create";
	public static final String INVENTORYMENU = "inventory-menu";
	
	
	public static final String QUEUED_DATE = "jqgh_dateCreated";
	public static final String SELECTDATE = "/html/body/div[3]/table/tbody/tr[2]/td[5]/a";
	public static final String SELECTMONTHFIELD = "/html/body/div[3]/div/a[2]/span";
	public static final String ADVANCEWARNING = "advanceWarning";
	public static final String SAVELOTSUPPLY = "save-lot-supply";
	public static final String EXPIRATIONDATE = "expirationDate";
	public static final String ACCOUNTABILITYUNITTYPE = "accountabilityUnitType";
	public static final String INITIALBALANCE = "initialBalance";
	public static final String MANUFACTURERLOTNUMBER = "manufacturerLotNumber";
	public static final String SPECIALLOTNUMBER = "specialLotNumber";
	public static final String CLICKONRECEIVE = "receive";
	public static final String LOWBALANCE = "lowBalance";
	public static final String CLICKONLOTNUMBER = "/html/body/div[1]/div[7]/div[2]/div[1]/div/div[3]/div[3]/div/table/tbody/tr[1]/td[2]/a";
	public static final String LOTSONORDER = "workqueue-lots-on-order";
	public static final String WORKQUEUEMENU = "workqueue-menu";
	
	public static final String STRENGTHUNITLISTBOX = "additionalInfoDrugDOs0.strengthUnit";
	public static final String DRUGSTRENGTH = "crisDrugStrength";
	public static final String CLICKNEXT3 = "next3";
	public static final String CLICKONSAVE = "save";
	public static final String CLICKONNEXT2 = "next2";
	public static final String PRESCRIBEDBY = "prescribedBy";
	public static final String CLICKADDDRUG = "addDrugName";
	public static final String DRUGNAMETEXT = "drugNameTextField";
	public static final String CLICKPROTOCOLNUMBER = "cpsProtocolNumber";
	public static final String CRISORDERNUMBER = "crisOrderNumber";
	public static final String MEDICALRECORDNUMBER = "medicalRecordNumber";
	public static final String CLICKNEXT = "/html/body/div[1]/div[7]/div[2]/form/fieldset/div[2]/fieldset/input";
	public static final String PRESCRIPTIONCREATE = "prescription-create";
	public static final String PRESCRIPTIONSMENU = "prescriptions-menu";
	public static final String RESETDRUG = "resetDrugNames";
	
	
	public static final String WORKQUEUE_MOUSEOVER_NAME = "workqueue-menu";
	public static final String LOTS_FOR_LABELLING = "workqueue-lots-for-labeling";
	public static final String LOT_NUMBER = "/html/body/div[1]/div[7]/div[2]/div[1]/div/div[3]/div[3]/div/table/tbody/tr[1]/td[2]/a";
	public static final String ADD_TO_STOCK = "/html/body/div[1]/div[7]/div[4]/div/div/div[7]/div[1]/div[3]/a[2]";
	public static final String QUANTITY = "quantity";
	public static final String NOTES = "note";
	public static final String SAVECLICK = "save";
	public static final String CONTINUE = "continue";
	
	public static final String SUBTRACT_FROM_STOCK = "/html/body/div[1]/div[7]/div[4]/div/div/div[7]/div[1]/div[3]/a[1]";
	public static final String DESTROY_STOCK = "/html/body/div[1]/div[7]/div[4]/div/div/div[7]/div[1]/div[2]/a[2]";
	public static final String RETURN_TO_STOCK = "/html/body/div[1]/div[7]/div[4]/div/div/div[7]/div[1]/div[2]/a[1]";
	public static final String TRANSFER = "/html/body/div[1]/div[7]/div[4]/div/div/div[7]/div[1]/div[1]/a";
	public static final String USE_IN_FORMULATION = "/html/body/div[1]/div[7]/div[4]/div/div/div[7]/div[2]/div[1]/a";
	public static final String FORMULATION_LOT = "formulationLot";
	public static final String CREATE_UNITDOSES = "/html/body/div[1]/div[7]/div[4]/div/div/div[7]/div[2]/div[2]/a";
	public static final String TRANSFER_QUANTITY = "initialBalance";
	public static final String AUDIT = "/html/body/div[1]/div[7]/div[4]/div/div/div[7]/div[2]/div[2]/a";
	public static final String ANNOTATION = "/html/body/div[1]/div[7]/div[4]/div/div/div[7]/div[2]/div[4]/a";
	public static final String DATE = "date";
	public static final String DATE_SELECT = "/html/body/div[3]/table/tbody/tr[2]/td[3]/a";
	
	public static final String PRESCRIPTION_FOOTER_SEARCH = "/html/body/div[2]/div[2]/div[1]/form/fieldset/div/input[3]";
	public static final String PRESCRIPTION_FOOTER_NUMBER = "qsPrescription";
	public static final String PROTOCOL_FOOTER_SEARCH = "/html/body/div[2]/div[2]/div[2]/form/fieldset/div/input[3]";
	public static final String PROTOCOL_FOOTER_NUMBER = "qsProtocol";
	public static final String DRUG_FOOTER_SEARCH = "/html/body/div[2]/div[2]/div[3]/form/fieldset/div/input[3]";
	public static final String DRUG_FOOTER_NUMBER = "qsDrug";
	public static final String LOT_FOOTER_SEARCH = "/html/body/div[2]/div[2]/div[4]/form/fieldset/div/input[3]";
	public static final String LOT_FOOTER_NUMBER = "qsLot";
	
	public static final String HOME = "/html/body/div[1]/div[3]/ul[1]/li[1]/a";
	public static final String FOOTER_MENU_BAR = "toggle_quicksearch_bar";
	
	public static final String SYSTEM = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/a";
	public static final String WORKLOAD_STATISTICS = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/ul/li[1]/a";
	public static final String WORKLOAD_STATISTICS_SEARCH = "/html/body/div[1]/div[7]/div[2]/form/fieldset/div[3]/input";
	public static final String IRREGULARITY_CONFIRMATIONS = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/ul/li[2]/a";
	public static final String IRREGULARITY_CONFIRMATIONS_SEARCH = "/html/body/div[1]/div[7]/div[2]/form/fieldset/div[3]/input";
	public static final String PREFERENCES = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/ul/li[4]/a";
	public static final String PREFERENCES_THEME = "selectedTheme6";
	public static final String PREFERENCES_THEME_SAVE = "save";	
	public static final String SYSTEMINFO = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/ul/li[5]/a";
	public static final String HELP = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/ul/li[6]/a";		
	public static final String ADMIN = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/ul/li[3]/a";
	public static final String MANAGE_USERS = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/ul/li[3]/ul/li[1]/a";
	public static final String MANAGE_USERS_SEARCH = "/html/body/div[1]/div[7]/div[2]/form/fieldset/div/table/tbody/tr[7]/td/input[1]";
	public static final String ADD_USER = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/ul/li[3]/ul/li[2]/a";
	public static final String CRIS_RX_PROCESS = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/ul/li[3]/ul/li[3]/a";
	public static final String MANAGE_DATATYPES = "/html/body/div[1]/div[5]/div[1]/ul/li[8]/ul/li[3]/ul/li[4]/a";
	
	
	
	
	


	

}
